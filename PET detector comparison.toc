\contentsline {chapter}{\numberline {1}A Survey of Current Photodetector Options for Positron Emission Tomography}{2}
\contentsline {section}{\numberline {1.1}Abstract}{2}
\contentsline {section}{\numberline {1.2}Introduction}{2}
\contentsline {subsubsection}{Classes of PET detector \cite {moses1994pet}}{3}
\contentsline {section}{\numberline {1.3}Scintillation-based PET photodetectors}{3}
\contentsline {subsection}{\numberline {1.3.1}Scintillation efficiency}{4}
\contentsline {subsection}{\numberline {1.3.2}Scintillator requirements}{4}
\contentsline {section}{\numberline {1.4}Photodetectors}{4}
\contentsline {subsection}{\numberline {1.4.1}Photomultiplier tube}{4}
\contentsline {subsection}{\numberline {1.4.2}Hybrid photodetectors}{6}
\contentsline {subsection}{\numberline {1.4.3}Solid state detectors}{6}
\contentsline {subsubsection}{Photodiodes}{7}
\contentsline {subsection}{\numberline {1.4.4}Avalanche Photodiodes}{8}
\contentsline {subsection}{\numberline {1.4.5}Geiger-mode Avalanche Photodiodes}{9}
\contentsline {subsection}{\numberline {1.4.6}Silicon photomultipliers}{9}
\contentsline {subsubsection}{Mode of operation}{9}
\contentsline {subsection}{\numberline {1.4.7}Digital silicon photomultipliers}{11}
\contentsline {subsection}{\numberline {1.4.8}High purity germanium detector}{11}
\contentsline {subsection}{\numberline {1.4.9}Silicon drift detectors}{13}
\contentsline {subsection}{\numberline {1.4.10}Silicon Strip Detectors}{13}
\contentsline {subsection}{\numberline {1.4.11}CVD diamond detectors}{14}
\contentsline {section}{\numberline {1.5}Gas-filled detectors (ionisation chambers)}{14}
\contentsline {subsection}{\numberline {1.5.1}Geiger-Mueller (GM) detector}{15}
\contentsline {section}{\numberline {1.6}Conclusion}{15}
